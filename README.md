# Introduction
This is an image file repository for testing QEMU.

# Usage
## Board: realview-pbx-a9
```
./build/arm-softmmu/qemu-system-arm -kernel ./images/zImage_realview_pbx_a9 -initrd ./images/rootfs.cpio -M realview-pbx-a9 -nographic -m 1024m
```

## Board: versatilepb
```
./build/arm-softmmu/qemu-system-arm -kernel ./images/zImage-versatilepb -initrd ./images/rootfs.cpio -append "console=ttyAMA0" -nographic -M versatilepb
```

## Board: arm-test
```
./build/arm-softmmu/qemu-system-arm -kernel ./arm-test/zImage.integrator -initrd ./arm-test/arm_root.img
```