#!/bin/bash

echo "Building llvm-2.8..."
tar -zxf llvm-2.8.tgz
cd llvm-2.8
patch -p1 < ../patch/llvm-2.8.patch
mkdir build
cd build
../configure --prefix=/usr/local/llvm-2.8 --enable-optimized
make -j4
sudo make install
export PATH=/usr/local/llvm-2.8/bin:$PATH
cd ../../

sleep 5;

echo "Building llvm-gcc..."
tar -zxf llvm-gcc-4.2-2.8.source.tgz
mkdir llvm-gcc-4.2-2.8.source/build
cd llvm-gcc-4.2-2.8.source/build
../configure --prefix=/usr/local/llvm-gcc --program-prefix=llvm- \
    --enable-llvm=/usr/local/llvm-2.8 --enable-languages=c,c++

if [ "$LIBRARY_PATH" == "" ]
then
    export LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/:$LIBRARY_PATH
else
    export LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/
fi

make -j4
sudo make install
export PATH=/usr/local/llvm-gcc/bin:$PATH
cd ../../


